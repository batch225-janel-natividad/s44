/*
    Fetch Method

    - The fetch () method in Javascript is used to request data from a server. The request can be of any type of APU that returns the data in JSON or XML

    Sample Fetching:

    fetch ('https://jsonplaceholder.typicode.com/posts') - API for the get request 
*/

fetch ('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data));

//Add post data
//Making post request using Fetch: Post request can be made using fetch by giving options as given below

document.querySelector('#form-add-post').addEventListener('submit' , (e) => {
    e.preventDefault();

    fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		}),
headers: {'Content-type': 'application/json; charset=UTF-8'}
	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added');

		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;

	})
})

//Show Post

const showPosts = (posts) => {
    let postEntries = '';

    posts.forEach((post) => {
        postEntries += 
        `<div id="post-${post.id}">
        <h3 id="post-title-${post.id}">${post.title} </h3>
        <p id="post-body-${post.id}">${post.body}</p>
        </div>
        <button id="edit-button-${post.id}" onclick="editPost(${post.id})">Edit</button>
        <button id="delete-button-${post.id}" onclick="deletePost(${post.id})">Delete</button>`
    });

    document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit Post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector('#txt-edit-id').value = id;
    document.querySelector('#txt-edit-title').value = title;
    document.querySelector('#txt-edit-body').value = body;

    document.querySelector('#btn-submit-update').removeAttribute('disabled');
}

//Update
    document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
        e.preventDefault();

        fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {'Content-type': 'application/json; charset=UTF-8' }
    })
    .then((response) => response.json())
    .then((data) => {
        console.log(data);
        alert('Successfully updated');


        document.querySelector('#txt-edit-id').value = null 
        document.querySelector('#txt-edit-title').value = null 
        document.querySelector('#txt-edit-body').value = null 

        // this code could be used to disable a submit button on a form, preventing the user from interacting with the button and submitting the form. 


        // When a submit button is disabled, it appears grayed out and cannot be click

        document.querySelector('#btn-submit-update').setAttribute('disabled', true);

    });
});

//Delete ACTIVITY
const deletePost = (postId) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
        method: 'DELETE'
    })
    .then(() => {
        document.querySelector(`#post-${postId}`).remove();
        document.querySelector(`#edit-button-${postId}`).remove();
        document.querySelector(`#delete-button-${postId}`).remove();
        alert(`Successfully deleted`);
    });
}

